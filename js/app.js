var tasks = [
'I Love Js',
'I Love Js',
'I Love Js',
'I need Lern Js ....;)'
];

// Varibles
var newTaskForm = document.querySelector('.new-task-container form')
var tasksContainer = document.querySelector('.tasks-container ul')


//On DOM load

document.addEventListener('DOMContentLoaded' ,function(){
	bindAddTaskEvent();
showTasks();
});


//Show task
function showTasks() {
	tasks.forEach(function(title){
	addNewTask(title);
})
}
//Toggle comete
function toggleTaskComplete(task){
   task.classList.toggle('btn-success');
}
//Delete task
function deleteTask(task){
  
  var liToDelete = task.closest ('li');
  task.closest('ul').removeChild(liToDelete);
}

function addNewTask(title){
	var taskLi = document.createElement('li');

	taskLi.classList.add('single-task');
	taskLi.innerHTML = prepareTaskHTML(title);
    
    //Events - toggle and delete

    var toggleComleteBtn = taskLi.querySelector('.toggle-complete-btn');
    var deleteBtn = taskLi.querySelector('.delete-task-btn');

    toggleComleteBtn.addEventListener('click' ,function() {
      toggleTaskComplete(this);
    });

    deleteBtn.addEventListener('click' ,function(){
       deleteTask(this);
    });

	//ADD TASK TO DOM//
	tasksContainer.appendChild(taskLi)
}



function prepareTaskHTML(title){
	
  return  '<div class="input-group">' +
           '<span class ="input-group-btn">' +
             '<button class="btn btn-default toggle-complete-btn"><i class="fas fa-check"></i></button>' +
            '</span>' +
                    '<input type="text" class="form-control"placeholder="Add" value="' + title + '">' +
                          
                '<span class ="input-group-btn">' +
            '<button class="btn btn-danger delete-task-btn"><i class="fas fa-trash-alt"></i></button>'+
                '</span>'+
    '</div>';
}


//Add New Task//dadaje zadania

function bindAddTaskEvent(){
 
 //On submit
 newTaskForm.addEventListener('submit', function(event) {
   event.preventDefault();

   var title = this.querySelector('input').value;
   if (title) {
      addNewTask(title);
}
 });
}